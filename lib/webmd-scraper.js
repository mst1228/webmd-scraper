const request = require('request-promise')
const cheerio = require('cheerio')
const bluebird = require('bluebird')
const async = bluebird.promisifyAll(require('async'))
const EventEmitter = require('events')
const constants = require('./constants.js')
const fs = require('fs')

const ALPHA_ORDER = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']

class WebMDScraper extends EventEmitter {
  constructor () {
    super()

    this._alphaIndex = 0
  }

  start () {
    fs.writeFileSync(constants.CONTENT_FILE, '')
    this._processAlphas()
      .then(() => {
        console.log('DONE!')
      })
      .catch(error => {
        console.log(error)
      })
  }

  _processAlphas () {
    if (this._alphaIndex < ALPHA_ORDER.length) {
      return this._getTopicURLsForAlpha(ALPHA_ORDER[this._alphaIndex])
        .then(urls => {
          return async.eachAsync(urls, (url, cb) => (this._getContentForTopicURL(url, cb)))
        })
        .then(() => {
          this._alphaIndex++
          return this._processAlphas()
        })
    } else {
      return Promise.resolve()
    }
  }

  _getContentForTopicURL (url, cb) {
    return this._getURL(constants.WEBMD_URL_BASE + url)
      .then(data => {
        console.log('GETTING TOPIC: ', url)
        this._handleTopicResponseData(data)
        cb(null)
      })
      .catch(error => {
        console.log('Error: ', url)
        if (cb) {
          cb(null)
        } else {
          return Promise.reject(error)
        }
      })
  }

  _getTopicURLsForAlpha (alpha) {
    console.log('GETTING ALPHA: ', alpha)
    const url = healthAZURLForAlpha(alpha)
    return this._getURL(url)
      .then(data => {
        const $ = cheerio.load(data)
        let urls = new Set()
        $('div.a-to-z.list ul li a').each((idx, el) => {
          urls.add($(el).attr('href'))
        })
        return Promise.resolve(urls)
      })
  }

  _getURL (url) {
    const opts = {
      uri: url,
      json: false
    }

    return request(opts)
  }

  _handleTopicResponseData (data) {
    const $ = cheerio.load(data)
    const $articleRdr = $('div.article_rdr')
    const $article = $('article')
    const $teaser = $('div.teaser_fmt')
    let content = ''
    if ($articleRdr.length) {
      const $textArea = $('#textArea')
      if ($textArea.length) {
        if ($articleRdr.hasClass('third_party')) {
          $textArea.find('p').slice(0, 2).remove()
          $textArea.find('p').last().remove()
        }
        $textArea.find('p').each((idx, el) => {
          let text = $(el).text().trim().replace(/\n\s*\n/gi, '\n') + ' '
          content += text
        })
        content += '\n'
      }
    } else if ($article.length) {
      $article.find('footer, .article-footer').remove()
      if ($article.hasClass('third_party') && $article.find('section').first().text().indexOf('Important') === 0) {
        $article.find('section').first().remove()
        $article.find('section').slice(-2).remove()
      } else {
        $article.find('section').last().remove()
      }
      $article.find('p').each((idx, el) => {
        let text = $(el).text().trim().replace(/\n\s*\n/gi, '\n') + ' '
        content += text
      })
      content += '\n'
    } else if ($teaser.length) {
      $teaser.find('p').each((idx, el) => {
        let text = $(el).text().trim().replace(/\n\s*\n/gi, '\n') + ' '
        content += text
      })
      content += '\n'
    } else {
      console.log('No articleRdr')
    }

    fs.appendFileSync(constants.CONTENT_FILE, content)
  }
}

function healthAZURLForAlpha (alpha) {
  alpha = alpha.toLowerCase()

  return (alpha === 'a') ? `${constants.WEBMD_HEALTH_AZ_URL_BASE}default.htm` : `${constants.WEBMD_HEALTH_AZ_URL_BASE}${alpha}.htm`
}

module.exports = WebMDScraper

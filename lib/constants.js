function createConstants () {
  let args = Array.prototype.slice.call(arguments)
  let constants = {}
  args.forEach((constant) => {
    if (Array.isArray(constant)) {
      let key = constant[0]
      let val = constant[1]
      constants[key] = val
    } else if (typeof constant === 'string') {
      constants[constant] = constant
    }
  })
  return constants
}

module.exports = createConstants(
  ['CONTENT_FILE', './CONTENT.txt'],
  ['WEBMD_URL_BASE', 'http://www.webmd.com/'],
  ['WEBMD_HEALTH_AZ_URL_BASE', 'http://www.webmd.com/a-to-z-guides/health-topics/']
)
